package com.ocss.message.custom;


import com.alibaba.fastjson.JSONObject;
import com.ocss.ocsscommon.message.KafkaMessage;
import com.ocss.message.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Auther: lijiang
 * @Date: 2023-04-17 20:06
 * @Description: MessageCustomer
 */

@Component
public class KafkaConsumerService {
    @Autowired
    MessageService messageService;
    // 消费监听
    @KafkaListener(id = "saveMessage",groupId = "my-group3", topics = {"visitorSend"})
    public void listen1(String data) throws IOException {
       KafkaMessage kafkaMessage =JSONObject.parseObject(data, KafkaMessage.class);
       messageService.InsertMessage(kafkaMessage);
    }
//    customerSend
    @KafkaListener(id = "saveSMessage",groupId = "my-group4", topics = {"customerSend"})
    public void listen2(String data) throws IOException {
        KafkaMessage kafkaMessage =JSONObject.parseObject(data, KafkaMessage.class);
        messageService.InsertMessage(kafkaMessage);
    }

}


