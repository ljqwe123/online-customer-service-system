package com.ocss.message.mapper;/*
@author yuxin
@version 1.0
*/

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ocss.ocsscommon.message.KafkaMessage;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessageMapper extends BaseMapper<KafkaMessage> {
//    Integer InsertMessage(KafkaMessage kafkaMessage);
}
