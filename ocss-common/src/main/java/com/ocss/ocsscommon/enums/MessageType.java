package com.ocss.ocsscommon.enums;

public enum MessageType {
    TEXT (5000),
    PHOTO(5001);


    private int code;
    MessageType(int code){
        this.code=code;
    }
}
