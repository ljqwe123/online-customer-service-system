package com.ocss.ocsscommon.message;


import com.ocss.ocsscommon.enums.MessageType;
import lombok.*;
import org.apache.kafka.common.protocol.types.Field;

/**
 * @Auther: lijiang
 * @Date: 2023-04-17 20:32
 * @Description: ClientMessage
 */
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientMessage {
    int type;
    String from;
    String to;
    String data;
}
