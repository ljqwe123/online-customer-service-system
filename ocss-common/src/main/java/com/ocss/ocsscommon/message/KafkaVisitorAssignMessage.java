package com.ocss.ocsscommon.message;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.protocol.types.Field;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 19:19
 * @Description: KafkaVisitorAssignMessage
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaVisitorAssignMessage {
    int commendType; //4000 表示请求为访客客服  4001 请求重分配  4002 访客离开   4009表示分配完成
    String Visitor;
    String VisitorIp;
    Integer VisitorPort;
    String VisitorSource;
    String Customer;
}
