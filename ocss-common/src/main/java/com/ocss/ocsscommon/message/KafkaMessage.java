package com.ocss.ocsscommon.message;


import com.baomidou.mybatisplus.annotation.TableName;
import com.ocss.ocsscommon.enums.MessageType;
import lombok.*;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: lijiang
 * @Date: 2023-04-12 20:02
 * @Description: MessageFormat
 */
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
//@TableName("t_message")
public class KafkaMessage{
    int type ;
    String playServerId;
    String sendId;
    String receiveId;
    Date sendTime;
    String messageSource;
    String data;
}
