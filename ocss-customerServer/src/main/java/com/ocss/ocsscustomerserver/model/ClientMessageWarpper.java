package com.ocss.ocsscustomerserver.model;


import lombok.*;

/**
 * @Auther: lijiang
 * @Date: 2023-05-06 22:23
 * @Description: ClientMessageWarpper
 */

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientMessageWarpper<E> {
    int messageType;  //6000 表示聊天    5000 表示会话建立   5001 表示会话状态变更
    E messageData;
}
