package com.ocss.ocsscustomerserver.model.ClientMessage;


import com.ocss.ocsscommon.message.KafkaVisitorAssignMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 21:12
 * @Description: ClientEstablishMessage
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientEstablishMessage   {
    String Visitor;
    String VisitorIp;
    Integer VisitorPort;
    String VisitorSource;
    String Customer;
}
