package com.ocss.ocsscustomerserver.model.ClientMessage;


import lombok.*;

/**
 * @Auther: lijiang
 * @Date: 2023-04-17 20:32
 * @Description: ClientMessage
 */
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientMessage {
    int type;
    String from;
    String to;
    String data;
}
