package com.ocss.ocsscustomerserver.tools;



@FunctionalInterface
public interface AllocationStrategy<T> {
    T run (String [] keys);
}
