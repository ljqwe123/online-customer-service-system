package com.ocss.ocsscustomerserver.tools;


import java.util.Random;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 19:46
 * @Description: PollAllocationStrategy
 */
public class PollAllocationStrategy  implements  AllocationStrategy{
    int i = 0;

    @Override
    public String run(String[] keys) {
        if (keys.length == 0) return  null;
        if (i >= keys.length) i = 0;
        String res = keys[i];
        i ++;
        return res;
    }
}
