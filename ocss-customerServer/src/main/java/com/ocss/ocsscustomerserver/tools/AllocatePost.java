package com.ocss.ocsscustomerserver.tools;


import com.alibaba.fastjson.JSONObject;
import com.ocss.ocsscommon.message.KafkaVisitorAssignMessage;
import com.ocss.ocsscustomerserver.model.ClientMessage.ClientEstablishMessage;
import com.ocss.ocsscustomerserver.model.ClientMessageWarpper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 20:05
 * @Description: AllocatePost
 */

@Component
public class AllocatePost {

    @Autowired
    WebsocketRecorder websocketRecorder;

    @Autowired
    MessagePost messagePost;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    public void  Allocate (KafkaVisitorAssignMessage kafkaVisitorAssignMessage) {



            // 具体分配策略
            String res = websocketRecorder.allocate();
            kafkaVisitorAssignMessage.setCustomer(res == null ? "-9999" : res);
            kafkaVisitorAssignMessage.setCommendType(4009);
            kafkaTemplate.send("assignComplete", JSONObject.toJSONString(kafkaVisitorAssignMessage));
            if (res == null) return;


            try {
                messagePost.informSessionEstablish(kafkaVisitorAssignMessage);
            }catch (Exception e) {
                e.printStackTrace();
            }


    }
}
