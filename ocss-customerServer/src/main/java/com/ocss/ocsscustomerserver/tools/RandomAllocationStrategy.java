package com.ocss.ocsscustomerserver.tools;


import java.util.Random;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 19:42
 * @Description: RandomAllocationStrategy
 */
public class RandomAllocationStrategy implements AllocationStrategy<String>{
    @Override
    public String run(String[] keys) {
        if (keys.length == 0) return  null;
        Random random = new Random();
        //随机产生0-9当中的一个随机整数
        int num = random.nextInt(keys.length);
        return keys[num];
    }
}
