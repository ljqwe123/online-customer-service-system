package com.ocss.ocsscustomerserver.tools;


import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: lijiang
 * @Date: 2023-04-18 19:28
 * @Description: AllocationStrategy
 */
@Component
//@ConditionalOnMissingBean(Allocation.class)
public class Allocation  {
    private String currentStrategy; //

    private Map<String, AllocationStrategy<String>> allocationStrategyMap = new HashMap<>(5);

    //初始化代码块
    {
        allocationStrategyMap.put("random", new RandomAllocationStrategy());
        allocationStrategyMap.put("poll", new PollAllocationStrategy());
        currentStrategy = "poll";
    }

    public void addStrategy (String StrategyName,  AllocationStrategy<String> Strategy) {
        allocationStrategyMap.put(StrategyName, Strategy);
    }


    public  void  setCurrentStrategy (String aimStrategy) {
        if (allocationStrategyMap.containsKey(aimStrategy)) {
            currentStrategy =  aimStrategy;
        }
    }


    public  String allocation(Map <String, WebSocketSession>  concurrentHashMap) {

        return allocationStrategyMap.get(currentStrategy).run(concurrentHashMap.keySet().toArray(new String[0]));
    }
}
