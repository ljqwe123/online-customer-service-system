package com.ocss.ocsscustomerserver.websocket;


import com.alibaba.fastjson.JSONObject;
import com.ocss.ocsscommon.enums.MessageType;
import com.ocss.ocsscommon.message.ClientMessage;
import com.ocss.ocsscommon.message.KafkaMessage;
import com.ocss.ocsscustomerserver.model.ClientMessageWarpper;
import com.ocss.ocsscustomerserver.tools.VerifyTokenUntil;
import com.ocss.ocsscustomerserver.tools.WebsocketRecorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;

import javax.annotation.Resource;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SocketServer  implements WebSocketHandler {
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    VerifyTokenUntil verifyTokenUntil;
    @Autowired
    WebsocketRecorder websocketRecorder;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String id = (String) session.getAttributes().get("id");
        websocketRecorder.putSession(id, session);
//        websocketRecorder.printAllSession();
        System.out.println("连接成功");
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        ClientMessageWarpper clientMessageWarpper = JSONObject.parseObject(message.getPayload().toString(), ClientMessageWarpper.class);
//        System.out.println(clientMessageWarpper);
        if (clientMessageWarpper.getMessageType() ==  6000) {
            ClientMessage clientMessage =JSONObject.parseObject(clientMessageWarpper.getMessageData().toString(), ClientMessage.class);;
            KafkaMessage kafkaMessage =  KafkaMessage.builder()
                .data(clientMessage.getData())
                .playServerId(websocketRecorder.getIdBySession(session))
                .receiveId(clientMessage.getTo())
                .sendId(clientMessage.getFrom())
                .sendTime(new Date())
                .type(clientMessage.getType()).build();
            kafkaTemplate.send("customerSend",JSONObject.toJSONString(kafkaMessage));
        }
//




//        MessageFormat messageFormat = new MessageFormat();
//        messageFormat.setData(message.getPayload().toString());
//        kafkaTemplate.send("customerSend",JSONObject.toJSONString(kafkaMessage));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {

    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        websocketRecorder.removeBySession(session);
//        websocketRecorder.printAllSession();
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
