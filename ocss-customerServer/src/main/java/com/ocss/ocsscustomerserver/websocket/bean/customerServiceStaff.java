package com.ocss.ocsscustomerserver.websocket.bean;


import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Auther: lijiang
 * @Date: 2023-04-12 17:19
 * @Description: customerServiceStaff
 */

@Data
public class customerServiceStaff {

    // 当前客服id
    String id;
    // 当前客服正在服务的人员数目
    AtomicInteger currentSeverCount;
}
