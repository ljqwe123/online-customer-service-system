package com.ocss.ocsscustomerserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = {"com.ocss.ocsscommon","com.ocss.ocsscustomerserver","com.ocss.ocssservice"})
public class OcssCustomerServerApplication {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(OcssCustomerServerApplication.class, args);
//        RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) context.getBean("redisTemplate");
//        redisTemplate.opsForHash().get("token",2);
    }
}
