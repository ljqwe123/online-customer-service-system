package com.ocss.ocsscustomerserver.config;

import com.ocss.ocsscustomerserver.interceptor.ConnectInterceptor;
import com.ocss.ocsscustomerserver.websocket.SocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;



/**
 * @Auther: lijiang
 * @Date: 2023-04-12 17:14
 * @Description:  开启webscoket功能，并配置Handler设置拦截器
 */
@Configuration
@EnableWebSocket
//@EnableWebSocketMessageBroker
public class MyConfiguration   implements WebSocketConfigurer {

    @Autowired
    SocketServer socketServer;
    @Autowired
    ConnectInterceptor connectInterceptor;


    // 配置Handler设置拦截器
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(socketServer, "/server").addInterceptors(connectInterceptor).setAllowedOrigins("*");
    }
}
