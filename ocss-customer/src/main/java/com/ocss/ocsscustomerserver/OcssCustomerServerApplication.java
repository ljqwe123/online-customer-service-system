package com.ocss.ocsscustomerserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcssCustomerServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OcssCustomerServerApplication.class, args);
    }
}
