package com.ocss.ocsscustomerserver.tools;


import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: lijiang
 * @Date: 2023-04-12 17:37
 * @Description: 验证此次websocket请求是否是登录状态
 */

@Component
public class VerifyTokenUntil {

    private   Map<String, String> recordTokenToIdCache = new HashMap(10);

    public    Boolean verifyToken (String token) {
        putTokenInfo(token, token);

        return  true;
    }

    // 验证令牌后缓存token对于的客服ID
    private  void putTokenInfo (String token, String id) {
        recordTokenToIdCache.put(token, id);
    }

    // 从缓存中取token对于的客服ID，并移出缓存
    public   String getIdByToken (String token) {
       return  recordTokenToIdCache.remove(token);
    }

}
