package com.ocss.ocsscustomerserver.tools;


import com.alibaba.fastjson.JSONObject;
import com.ocss.ocsscommon.message.ClientMessage;
import com.ocss.ocsscommon.message.KafkaVisitorAssignMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * @Auther: lijiang
 * @Date: 2023-04-17 21:08
 * @Description: MessagePost
 */

@Component
public class MessagePost {
    @Autowired
    WebsocketRecorder websocketRecorder;

    public  void  sendMessage(ClientMessage clientMessage) throws IOException {
        WebSocketSession session = websocketRecorder.getSession(clientMessage.getTo());
        if (session == null) {
            System.out.println("无目标用户");
        }
        session.sendMessage(new TextMessage(JSONObject.toJSONString(clientMessage)));
    }
    public  void  sendEstablish(KafkaVisitorAssignMessage establish) throws IOException {
        WebSocketSession session = websocketRecorder.getSession(establish.getVisitor());
        if (session == null) {
            System.out.println("无目标用户");
        }
        session.sendMessage(new TextMessage(JSONObject.toJSONString(establish)));
    }

}
