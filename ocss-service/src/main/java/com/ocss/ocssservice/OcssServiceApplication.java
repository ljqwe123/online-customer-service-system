package com.ocss.ocssservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.ocss.ocssservice", "com.ocss.ocsscommon"})
//@ComponentScan()
public class OcssServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OcssServiceApplication.class, args);
//        System.out.println(UUID.randomUUID().toString());
    }

}
