package com.ocss.ocssservice.mapper;/*
@author yuxin
@version 1.0
*/


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ocss.ocssservice.entity.Server;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface ServerMapper extends BaseMapper<Server> {
    Server selectByIdAndPassword(String serverId,String password);
    Server selectByServerId(String serverId);
}
