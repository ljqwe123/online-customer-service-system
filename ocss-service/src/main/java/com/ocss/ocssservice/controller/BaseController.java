package com.ocss.ocssservice.controller;

import javax.servlet.http.HttpSession;

/**
 * 控制层里面类的基类
 */
public class BaseController {

    //操作成功的状态码
    public static final int OK = 200;

    /**
     * 获取session对象中的uid
     * @param session session对象
     * @return 当前登录的用户uid的值
     */
    public final Integer getUidFromSession(HttpSession session) {
        //getAttribute返回的是Object对象,需要转换为字符串再转换为包装类
        return Integer.valueOf(session.getAttribute("uid").toString());
    }

    public final String getUsernameFromSession(HttpSession session) {
        return session.getAttribute("username").toString();
    }
}
