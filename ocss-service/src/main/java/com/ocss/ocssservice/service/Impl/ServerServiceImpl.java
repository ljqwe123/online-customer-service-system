package com.ocss.ocssservice.service.Impl;/*
@author yuxin
@version 1.0
*/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ocss.ocssservice.entity.Server;
import com.ocss.ocssservice.mapper.ServerMapper;
import com.ocss.ocssservice.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServerServiceImpl extends ServiceImpl<ServerMapper, Server> implements ServerService {
    @Autowired
    ServerMapper serverMapper;
    @Override
    public Server selectServerByIdAndPassword(String serverId, String password) {
        return serverMapper.selectByIdAndPassword(serverId,password);
    }

    @Override
    public Server selectServerById(String serverId) {
        return serverMapper.selectByServerId(serverId);
    }

}
