package com.ocss.ocssservice.service.Impl;/*
@author yuxin
@version 1.0
*/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ocss.ocsscommon.message.KafkaMessage;
import com.ocss.ocssservice.mapper.MessageMapper;
import com.ocss.ocssservice.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper,KafkaMessage> implements MessageService {
    @Autowired
    MessageMapper messageMapper;

    @Override
    public Integer InsertMessage(KafkaMessage message) {
        return messageMapper.insert(message);
    }
}
