package com.ocss.ocssservice.service;/*
@author yuxin
@version 1.0
*/

import com.baomidou.mybatisplus.extension.service.IService;
import com.ocss.ocssservice.entity.Server;
import org.springframework.stereotype.Service;

@Service
public interface ServerService extends IService<Server> {
    Server selectServerByIdAndPassword(String serverId,String password);
    Server selectServerById(String serverId);
}
