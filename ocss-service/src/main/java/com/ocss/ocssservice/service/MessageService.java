package com.ocss.ocssservice.service;/*
@author yuxin
@version 1.0
*/

import com.baomidou.mybatisplus.extension.service.IService;
import com.ocss.ocsscommon.message.KafkaMessage;
import org.springframework.stereotype.Service;

@Service
public interface MessageService extends IService<KafkaMessage> {

    Integer InsertMessage(KafkaMessage message);
}
