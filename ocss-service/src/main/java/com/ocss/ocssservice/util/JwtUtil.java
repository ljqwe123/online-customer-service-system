package com.ocss.ocssservice.util;/*
@author yuxin
@version 1.0
*/


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.Date;
import java.util.HashMap;

public class JwtUtil {
    private static final long EXPIRE_TIME = 15 * 60 * 100;
    private static final String TOKEN_SECRET = "a06d3bf3-125a-4c0c-ab5a-9c8419f495b4";

    public static String sign(String serverId,String passWord){
        Date date = new Date(System.currentTimeMillis()+ EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
        HashMap<String, Object> header = new HashMap<>(2);
        header.put("type","JWT");
        header.put("alg","HS256");
        return JWT.create()
                .withHeader(header)
                .withClaim("ServerId",serverId)
                .withClaim("password",passWord)
                .withExpiresAt(date)
                .sign(algorithm);
    }
}
