package com.ocss.ocssservice.util;/*
@author yuxin
@version 1.0
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisSave {
    @Autowired
    RedisTemplate redisTemplate;

    public void saveServerId(String token,String ServerId){
        redisTemplate.opsForValue().set(token,ServerId);
    }

}
