package com.ocss.ocssservice.entity;/*
@author yuxin
@version 1.0
*/

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class ServerRO {
    private String serverId;
    private String name;
    private String passWord;
    private String token;
}
