package com.ocss.ocssservice.entity;/*
@author yuxin
@version 1.0
*/

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Server implements Serializable {
    private Integer id;
    private String serverId;
    private String name;
    private String passWord;
    private String sex;
    private String image;
    private Integer isForbidden;
    private Integer isDelete;
    private Date registerTime;
    private String autoReply;

}
